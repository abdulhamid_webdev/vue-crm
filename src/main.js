import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import messagePlugin from './utils/message.plugin'
import firebase from 'firebase/app'
import currencyFilter from './filters/currency.filter'
import Loader from '@/components/app/Loader'
import toolTipDirective from '@/directive/tooltip.directive'
import Paginate from 'vuejs-paginate'
import localizeFilter from './filters/localize.filter'
import './registerServiceWorker'
import 'materialize-css/dist/js/materialize.min'

import 'firebase/auth'
import 'firebase/database'
Vue.config.productionTip = false
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.filter('localize',localizeFilter)
Vue.use(messagePlugin)
Vue.directive('tooltip', toolTipDirective)
Vue.component('Loader', Loader)
Vue.component('Paginate', Paginate)
let app
firebase.initializeApp({
  apiKey: "AIzaSyAHZP0B-a2csZGpuiUVgpnepEYT46LKVQ4",
  authDomain: "vue-test-bd.firebaseapp.com",
  databaseURL: "https://vue-test-bd.firebaseio.com",
  projectId: "vue-test-bd",
  storageBucket: "vue-test-bd.appspot.com",
  messagingSenderId: "112612427812",
  appId: "1:112612427812:web:29f307caa7858b71ad70b1",
  measurementId: "G-SM51C9YQ7M"
})
firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
